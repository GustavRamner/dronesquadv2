function [dx, dr] = findPath(bin_image)
    
    r = 1; % Downsampling step size
    dy = 20; % Step-size in y
    
    small = bin_image(1:r:end, 1:r:end);
    S = size(small);
    [X,Y] = meshgrid(1:S(2), 1:S(1));
    
    % This function estimates path's x-value at a given y-value:
    function x = get_x(y)
        slice = sum( small( y-dy:y+dy, : ), 1);
        x = sum(slice .* X(1,:)) / sum(slice);
    end
    
    % Pick a y-value representing drone position:
    middle_row = round(S(1)/2);
    
    c = get_x(middle_row);
    m = ( get_x(middle_row+dy)-get_x(middle_row-dy) ) / (2*r*dy);
    
    dx = (c-S(2)/2) * r;
    dr = atan(m);
    
end