function monochrome = extractColor(color, img)
    r = img(:,:,1);
    g = img(:,:,2);
    b = img(:,:,3);
    
    monochrome = r; % Imply type for code generation
    
    switch color
        case 'red'
            not_c = imcomplement( (g+b)./2 );
            monochrome = r .* not_c;
        case 'green'
            not_c = imcomplement( (r+b)./2 );
            monochrome = g .* not_c;
        case 'blue'
            not_c = imcomplement( (r+g)./2 );
            monochrome = b .* not_c;
    end
    
    % Pre-blur for cleaner results:
    monochrome = imgaussfilt(monochrome, 0.1);
end