function circles = findCircle(monochrome)
    %{
    r = 2; % Downsampling step size
    small = monochrome(1:r:end, 1:r:end);
    
    % Find the circles in the image
    % Change the radiusRange appropriately (depends on the video)
    radiusRange = [80 120] / r;
    [centers, radius] = imfindcircles(small, radiusRange);
    
    if ~isempty(radius)
        [~,i] = max(radius);
        i = i(1);
        circles = [centers(i,1)*r, centers(i,2)*r, radius(i)*r];
    else
        circles = [0,0,0];
    end
    %}
    circles = [0,0,0];
end